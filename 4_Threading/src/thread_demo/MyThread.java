package thread_demo;

public class MyThread extends Thread{
	
	private long delay;
	
	private String description;

	public MyThread(long delay, String description) {
		this.delay =delay;
		this.description = description;
	}
	
	public void run() {
		System.out.println("Start mythread. Description = " + description);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.println("End mythread");
	}
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");
		MyThread t1 = new MyThread(1000, "This is the first thread");
		t1.start();
		MyThread t2 = new MyThread(3000, "This is the second thread");
		t2.start();
		t1.join();
		t2.join();
		System.out.println("End");
	}
}
