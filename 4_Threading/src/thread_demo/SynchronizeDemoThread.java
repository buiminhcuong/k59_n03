package thread_demo;

public class SynchronizeDemoThread extends Thread {
	private Printer printer;
	private int n;

	public SynchronizeDemoThread(int n, Printer printer)  {
		this.n = n;
		this.printer = printer;
	}
	
	public void run() {
		printer.print(n);
	}

	public static void main(String[] args) {
		Printer printer = new Printer();
		SynchronizeDemoThread t1 = new SynchronizeDemoThread(5, printer);
		t1.start();
		
		SynchronizeDemoThread t2 = new SynchronizeDemoThread(10, printer);
		t2.start();
	}

}
