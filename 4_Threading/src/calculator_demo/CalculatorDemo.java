package calculator_demo;

public class CalculatorDemo {
	public static void main(String[] args) {
		System.out.println("Start");
		
		long start = System.currentTimeMillis();
		long result = sum();
		long end = System.currentTimeMillis();
		
		System.out.println("Sum = " + result + "; Total time: " + (end - start) + "ms");
		System.out.println("End");
	}
	
	public static long sum() {
		long result = 0;
		for (int i = 0; i < 1000000000; i++) {
			result += i;
		}
		return result;
	}
}
