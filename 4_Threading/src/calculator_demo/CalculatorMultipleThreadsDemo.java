package calculator_demo;

public class CalculatorMultipleThreadsDemo {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");

		long start = System.currentTimeMillis();

		int numberOfThreads = 10;
		int from = 0;
		int to = 1000000000;
		int step = (to - from) / numberOfThreads;

		CalculatorThread[] threads = new CalculatorThread[numberOfThreads];

		for (int i = 0; i < threads.length; i++) {
			threads[i] = new CalculatorThread(step * i, step * (i + 1));
			threads[i].start();
		}

		long sum = 0;
		for (int i = 0; i < threads.length; i++) {
			threads[i].join();
			sum += threads[i].getSum();
		}

		long end = System.currentTimeMillis();

		System.out.println("Sum = " + sum + "; Total time: " + (end - start) + "ms");
		System.out.println("End");

	}

}
