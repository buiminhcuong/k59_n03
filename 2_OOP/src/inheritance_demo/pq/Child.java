package inheritance_demo.pq;

public class Child extends Parent{
	protected void printQ() {
		System.out.println("Q2");
	}
	
	public void printPQ() {
		printP();
		super.printQ();
	}
}
