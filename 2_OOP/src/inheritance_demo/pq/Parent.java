package inheritance_demo.pq;

public class Parent {
	protected void printP() {
		System.out.println("P1");
	}
	
	protected void printQ() {
		System.out.println("Q1");
	}
	
	public void printPQ() {
		printP();
		printQ();
	}
}
