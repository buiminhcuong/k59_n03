package inheritance_demo;

public class Dog extends Animal{
	public void sound() {
		System.out.println("Gau gau");
	}
	
	public void eat() {
		System.out.println("Eating");
	}
}
