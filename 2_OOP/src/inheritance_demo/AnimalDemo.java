package inheritance_demo;

public class AnimalDemo {
	public static void main(String[] args) {
		Animal[] a = new Animal[3];
		a[0] = new Dog();
		a[1] = new Cat();
		a[2] = new Dog();
		
		for(int i = 0; i < a.length; i++) {
			a[i].sound();
			if(a[i] instanceof Dog) {
				((Dog) a[i]).eat();
			}
			else if(a[i] instanceof Cat) {
				((Cat) a[i]).climb();
			}
			System.out.println("----------");
		}
	}
}
