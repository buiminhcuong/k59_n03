package inheritance_demo;

public class Cat extends Animal{
	public void sound() {
		System.out.println("Meo meo");
	}
	
	public void climb() {
		System.out.println("climbing");
	}
}
