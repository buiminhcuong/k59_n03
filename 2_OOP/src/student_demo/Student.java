package student_demo;

import java.security.InvalidParameterException;

public class Student {
	private String name;
	private float math;
	private float physics;
	private float chemistry;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getMath() {
		return math;
	}

	public void setMath(float math) {
		checkMark(math);
		this.math = math;
	}

	public float getPhysics() {
		return physics;
	}

	public void setPhysics(float physics) {
		checkMark(physics);
		this.physics = physics;
	}

	public float getChemistry() {
		return chemistry;
	}

	public void setChemistry(float chemistry) {
		checkMark(chemistry);
		this.chemistry = chemistry;
	}

	public float avg() {
		return (math + physics + chemistry) / 3;
	}
	
	public void print() {
		System.out.println("Student: " + name + "; math: " + math + "; physic: " + physics + "; chemistry" + chemistry);
	}
	
	private void checkMark(float value) {
		if(value > 10 || value <= 0) {
			throw new InvalidParameterException("Mark must be between [0;10]");
		}
	}
}
