package diem_demo;

import java.util.Scanner;

public class DiemDemo {

	public static void main(String[] args) {
		Diem d1 = inputDiem();
		Diem d2 = inputDiem();
		
		DuongThang dt = new DuongThang(d1, d2);
		System.out.println("Do dai duong thang: " + dt.doDai());
	}

	private static Diem inputDiem() {
		System.out.println("Nhap diem [x, y]: ");
		Scanner sc = new Scanner(System.in);
		System.out.print("x: ");
		double x = sc.nextDouble();
		System.out.print("y: ");
		double y = sc.nextDouble();
		Diem d1 = new Diem(x, y);
		return d1;
	}

}
