package inheritance_constructor_demo;

public class Parent{
	public String p1;
	
	public Parent(String p1) {
		this.p1 = p1;
		System.out.println("Parent constructor");
	}
}
