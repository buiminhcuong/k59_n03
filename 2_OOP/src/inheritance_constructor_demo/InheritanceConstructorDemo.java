package inheritance_constructor_demo;

public class InheritanceConstructorDemo {

	public static void main(String[] args) {
		Children c1 = new Children("property 1", 5);
		System.out.println("p1 = " + c1.p1 + "; p2 = " + c1.p2);
	}
}
