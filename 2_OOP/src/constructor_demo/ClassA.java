package constructor_demo;

public class ClassA {
	public int property1;
	
	public ClassA(int property1) {
		this.property1 = property1;
	}
	
	public ClassA() {
		
	}
	
	public void ClassA() {
		
	}
}
