package constructor_demo;

public class ConstructorDemo {

	public static void main(String[] args) {
		ClassA a1 = new ClassA(5);
		System.out.println(a1.property1);
		
		ClassA a2 = new ClassA();
		System.out.println(a2.property1);
	}
}
