package static_demo;

public class MyClassDemo {
	public static void main(String[] args) {
		MyClass c1 = new MyClass();
		System.out.println(c1.i);
		
		MyClass c2 = new MyClass();
		System.out.println(c2.i);
		
		MyClass c3 = new MyClass();
		System.out.println(MyClass.i);
		
		MyClass.method1();
	}
}
