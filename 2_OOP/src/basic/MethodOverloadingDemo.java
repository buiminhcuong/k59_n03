package basic;

public class MethodOverloadingDemo {

	public static void main(String[] args) {
		method1();
		method1("");
		method1(0);
	}
	
	public static void method1() {
		System.out.println("method1");
	}
	
	public static void method1(String s) {
		System.out.println("method1(String s)");
	}
	
	public static void method1(int i) {
		System.out.println("method1(int i)");
	}
}
