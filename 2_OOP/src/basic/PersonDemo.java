package basic;

public class PersonDemo {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.id = 1;
		
		changeit1(p1);
		System.out.println(p1.id);
	}
	
	public static void changeit1(Person p) {
		p.id = 5;
	}
	
	public static void changeit2(int i) {
		i = 10;
	}
}
