package package1;

public class Class1 {
	public void method1() {
		System.out.println("Class1.method1");
	}
	
	protected void method2() {
		System.out.println("Class1.method2");
	}
	
	private void method3() {
		System.out.println("Class1.method3");
	}
	
	void method4() {
		System.out.println("Class1.method4");
	}
}
