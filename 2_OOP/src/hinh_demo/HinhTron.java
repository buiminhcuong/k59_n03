package hinh_demo;

import diem_demo.Diem;

public class HinhTron implements Hinh{
	private double r;
	private Diem o;
	
	public HinhTron(Diem o, double r) {
		super();
		this.o = o;
		this.r = r;
	}
	
	public double chuVi() {
		return 2 * Math.PI + r;
	}
	
	public double dienTich() {
		return Math.PI * r * r;
	}
	
	public String toString() {
		return "Hinh Tron: O=" + o + "; R =" + r;
	}
}
