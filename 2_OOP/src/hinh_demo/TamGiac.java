package hinh_demo;

import diem_demo.Diem;
import diem_demo.DuongThang;

public class TamGiac implements Hinh{
	private Diem a;
	private Diem b;
	private Diem c;
	private double ab;
	private double bc;
	private double ca;
	
	public TamGiac(Diem a, Diem b, Diem c) {
		this.a = a;
		this.b = b;
		this.c = c;
		
		//kiem tra 3 diem thang hang
		ab = new DuongThang(a, b).doDai();
		bc = new DuongThang(b, c).doDai();
		ca = new DuongThang(c, a).doDai();
	}
	
	public double chuVi() {
		return ab + bc + ca;
	}
	
	public double dienTich() {
		double p = chuVi() / 2;
		return Math.sqrt(p* (p - ab) * (p - bc) * (p - ca)); 
	}
	
	public String toString() {
		return "Tam Giac: " + a.toString() + "; " + b.toString() + "; " + c.toString();
	}
}
