package hinh_demo;

public class ChuNhat implements Hinh{
	private double a;
	private double b;
	
	public ChuNhat(double a, double b) {
		this.a = a;
		this.b = b;
	}
	
	public double chuVi() {
		return 2 * (a + b);
	}
	
	public double dienTich() {
		return a * b;
	}
	
	public String toString() {
		return "Chu Nhat: " + "a = " + a + "; b =" + b; 
	}
}
