package hinh_demo;

import java.util.Scanner;

import diem_demo.Diem;

public class HinhDemo {

	public static void main(String[] args) {
		int n = readInt("Bạn muốn nhập vào bao nhiêu hình: ", 1, 10);

		Hinh[] hinh = new Hinh[n];

		for (int i = 0; i < n; i++) {
			int choice = readInt("Nhập vào lựa chọn của bạn (1. TamGiac, 2. HinhTron, 3.ChuNhat): ", 1, 3);

			switch (choice) {
			case 1:
				Diem a = readDiem("Nhập vào đỉnh A: ");
				Diem b = readDiem("Nhập vào đỉnh B: ");
				Diem c = readDiem("Nhập vào đỉnh C: ");
				hinh[i] = new TamGiac(a, b, c);
				break;
			case 2:
				Diem o = readDiem("Nhập vào tâm O: ");
				double r = readDouble("Nhập vào bán kính r", 0, Long.MAX_VALUE);
				hinh[i] = new HinhTron(o, r);
				break;
			case 3:
				double a1 = readDouble("Chiều dài cạnh a: ", 0, Long.MAX_VALUE);
				double b1 = readDouble("Chiều dài cạnh b: ", 0, Long.MAX_VALUE);
				hinh[i] = new ChuNhat(a1, b1);
				break;
			default:
				break;
			}
		}

		System.out.println("Danh sách hình");
		for(int i = 0; i < hinh.length; i++) {
			printHinh(hinh[i]);
		}
	}

	private static int readInt(String message, int lowerBound, int upperBound) {
		Scanner sc = new Scanner(System.in);
		int result;
		do {
			System.out.print(message);
			result = sc.nextInt();

			if (result < lowerBound || result > upperBound) {
				System.out.println("Dữ liệu nằm ngoài khoảng cho phép");
			}
		} while (result < lowerBound || result > upperBound);

		return result;
	}
	
	private static double readDouble(String message, double lowerBound, double upperBound) {
		Scanner sc = new Scanner(System.in);
		double result;
		do {
			System.out.print(message);
			result = sc.nextDouble();

			if (result < lowerBound || result > upperBound) {
				System.out.println("Dữ liệu nằm ngoài khoảng cho phép");
			}
		} while (result < lowerBound || result > upperBound);

		return result;
	}
	
	private static Diem readDiem(String message) {
		Scanner sc = new Scanner(System.in);
		System.out.println(message);

		System.out.print("x: ");
		double x = sc.nextDouble();
		
		System.out.print("y: ");
		double y = sc.nextDouble();
		
		return new Diem(x, y);
	}

	private static void printHinh(Hinh h) {
		System.out.println(h.toString());
		System.out.println("Chu vi: " + h.chuVi());
		System.out.println("Dien tich: " + h.dienTich());
	}
}
