package io_demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy1 {

	public static void main(String[] args) throws IOException {
		try(FileInputStream fi = new FileInputStream("C:\\java\\file1.txt");
			FileOutputStream fo = new FileOutputStream("C:\\java\\file2.txt")) {
			int c;
			while((c = fi.read()) != -1) {
				fo.write(c);
			}
		}
		System.out.println("Done");
	}

}
