package io_demo;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCopy2 {

	public static void main(String[] args) throws IOException {
		try(FileReader fr = new FileReader("C:\\java\\file1.zip");
			FileWriter fw = new FileWriter("C:\\java\\file2.zip")) {
			int c;
			while((c = fr.read()) != -1) {
				fw.write(c);
			}
		}
		System.out.println("Done");
	}

}
