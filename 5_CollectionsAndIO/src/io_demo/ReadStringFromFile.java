package io_demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadStringFromFile {

	public static void main(String[] args) throws IOException {
		byte[] bytes = Files.readAllBytes(Paths.get("C:\\java\\file1.txt"));
		String s = new String(bytes);
		System.out.println(s);
	}

}
