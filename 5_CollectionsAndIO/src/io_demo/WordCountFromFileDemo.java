package io_demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import collection_demo.WordCountDemo;

public class WordCountFromFileDemo {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter file path: ");
		String filePath = sc.nextLine();
		
		byte[] bytes = Files.readAllBytes(Paths.get(filePath));
		String s = new String(bytes);
		
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		wordCount = WordCountDemo.countWords(s);
		
		for(String word : wordCount.keySet()) {
			System.out.println(word + ": " + wordCount.get(word));
		}
	}
}
