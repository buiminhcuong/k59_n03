package collection_demo;

import java.util.HashMap;
import java.util.Map;

public class WordCountDemo {
	public static void main(String[] args) {
		//TODO: Read input from user
		Map<String, Integer> wordCounts = countWords("hello and welcome to Java. Hello");
		
		for(String word : wordCounts.keySet()) {
			System.out.println(word + ": " + wordCounts.get(word));
		}
	}
	
	public static Map<String, Integer> countWords(String s) {
		String[] words = s.split(" ");
		Map<String, Integer> wordCounts = new HashMap<String, Integer>();
		
		
		for(String word : words) {
			int count = 1;
			if(wordCounts.containsKey(word)) { 
				count += wordCounts.get(word);
			}
			wordCounts.put(word, count);
		}
		
		return wordCounts;
	}
}
