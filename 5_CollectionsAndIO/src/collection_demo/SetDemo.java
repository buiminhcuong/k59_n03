package collection_demo;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		Set<Person> people = new HashSet<Person>();
		
		people.add(new Person(1, "An"));
		people.add(new Person(2, "Binh"));
		System.out.println(people.size());
		
		people.add(new Person(1, "An"));
		System.out.println(people.size());

	}

}
