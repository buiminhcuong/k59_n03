package collection_demo;

import java.util.ArrayList;
import java.util.List;

public class IndexOfDemo2 {
	public static void main(String[] args) {
		List<Person> list = new ArrayList<Person>();
		list.add(new Person(1, "An"));
		list.add(new Person(2, "Binh"));
		//------------------------------------
		Person seek = new Person(2, "Binh");
		
		System.out.println(list.indexOf(seek));
	}
}
