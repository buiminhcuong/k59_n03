package collection_demo;

import java.util.ArrayList;
import java.util.List;

public class IndexOfDemo {

	public static void main(String[] args) {
		int[] arr1 = new int[10];
		arr1[0] = 43;
		arr1[1] = 29;
		arr1[2] = 27;
		arr1[5] = 10;
		
		int index = -1;
		int seek = 27;
		for (int i = 0; i < arr1.length; i++) {
			if(arr1[i] == seek) {
				index = i;
				break;
			} 
		}
		
		System.out.println("Index of " + seek + " is " + index);
		
		List<Integer> list1 = new ArrayList<Integer>();
		list1.add(43);
		list1.add(29);
		list1.add(27);
		list1.add(10);
		System.out.println("Index of " + seek + " is " + list1.indexOf(seek));
	}

}

