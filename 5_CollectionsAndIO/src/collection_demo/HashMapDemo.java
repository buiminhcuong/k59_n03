package collection_demo;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo {

	public static void main(String[] args) {
		Map<String, Integer> words = new HashMap<String, Integer>();
		words.put("Hello", 1);
		words.put("world", 1);
		words.put("welcome", 1);
	
		for (String key : words.keySet()) {
			System.out.println("key = " + key + "; value = " + words.get(key));
		}
	}
}
