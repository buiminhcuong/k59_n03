package collection_demo;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		String[] sArr1 = new String[3];
		sArr1[0] = "Hello";
		sArr1[1] = "and";
		sArr1[2] = "welcome";
		
		for (int i = 0; i < sArr1.length; i++) {
			System.out.println(sArr1[i]);
		}
		
		System.out.println("------");
		for(String s : sArr1) {
			System.out.println(s);
		}
		
		System.out.println("-----------------------------");
		List<String> list1 = new ArrayList<String>();
		list1.add("Hello");
		list1.add("and");
		list1.add("welcome");
		for (int i = 0; i < list1.size(); i++) {
			System.out.println(list1.get(i));
		}
		
		System.out.println("------");
		for(String s : list1) {
			System.out.println(s);
		}
	}

}
