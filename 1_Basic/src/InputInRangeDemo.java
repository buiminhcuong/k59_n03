import java.util.Scanner;

public class InputInRangeDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a;
		do {
			System.out.println("Input a(0-100):");
			a = sc.nextInt();
		}while(a<0||a>100);
		System.out.println("The number: "+a);
		sc.close();
	}

}
