import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Input a: ");
		int a = sc.nextInt();
		
		System.out.print("Input b: ");
		int b = sc.nextInt();
		
		System.out.println("Sum: " + (a + b));
	}
}
