
public class ArrayDemo {

	public static void main(String[] args) {
		int[] arr1 = new int[10];
		arr1[0] = 10;
		arr1[1] = 12;
		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1[2]);

		int[] arr2 = new int[] { 5, 6, 7, 8, 9 };
		
		System.out.println(arr2[3]);
	}
}
