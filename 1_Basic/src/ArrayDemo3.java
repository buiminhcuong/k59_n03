import java.util.Scanner;

public class ArrayDemo3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Moi nhap so chu so: ");
		int size = sc.nextInt();
		int elements[] = new int[size];
		for (int i = 0; i < size; i++) {
			System.out.print("Moi nhap phan tu thu " + i + ": ");
			elements[i] = sc.nextInt();
		}
		System.out.print("Danh sach cac so la: ");
		for (int i = 0; i < size; i++) {
			System.out.print(elements[i] + " ");
		}
		int total = 0;
		for (int i = 0; i < size; i++) {
			total += elements[i];
		}
		System.out.println("Tong la: " + total);
	}

}
