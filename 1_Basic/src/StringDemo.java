
public class StringDemo {

	public static void main(String[] args) {
		String s1 = "abc";
		System.out.println(s1.equals("abc"));
		
		//avoid using this
		String s2 = new String("abc");
		System.out.println(s2.equals("abc"));
		
		//avoid using this
		String s3 = new String("abc");
		System.out.println(s2.equals(s3));
		
		String s4 = "abc";
		System.out.println(s1.equals(s4));
	}

}
