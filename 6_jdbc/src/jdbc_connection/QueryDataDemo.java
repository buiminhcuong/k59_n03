package jdbc_connection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryDataDemo {
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		Map<String, String> config = readConfig();
		
		//B1: Nap driver
		Class.forName(config.get("JDBC_CLASS"));
		
		try(
			//B2: Mo ket noi
			Connection conn = DriverManager.getConnection(config.get("JDBC_URL"), config.get("USERNAME"), config.get("PASSWORD"));
			Statement stm = conn.createStatement()) {
			//B3: Thuc hien truy van
			ResultSet result = stm.executeQuery("SELECT * FROM Person");
			//B4: Xu ly ket qua
			while(result.next()) {
				System.out.println(result.getInt("id"));
				System.out.println(result.getString("name"));
				System.out.println(result.getString("address"));
			}
			result.close();
		}
	}

	private static Map<String, String> readConfig() throws IOException {
		List<String> configText = Files.readAllLines(Paths.get("config.txt"));
		Map<String, String> config = new HashMap<String,String>();
		for(String line : configText) {
			String[] arr = line.split("=", 2);
			String key = arr[0];
			String value = "";
			if(arr.length > 1) {
				value = arr[1];
			}
			config.put(key, value);
		}
		return config;
	}
}
