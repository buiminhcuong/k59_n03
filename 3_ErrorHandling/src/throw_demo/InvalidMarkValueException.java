package throw_demo;

public class InvalidMarkValueException extends Exception{
	public InvalidMarkValueException(String message) {
		super(message);
	}
}
