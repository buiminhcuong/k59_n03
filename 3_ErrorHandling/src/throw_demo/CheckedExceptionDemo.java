package throw_demo;

import java.io.IOException;

public class CheckedExceptionDemo {

	public static void main(String[] args) throws IOException{
		throwDemo();
		try {
			sleepDemo();
		}
		catch (InterruptedException e) {
			System.out.println("sleep error");
			throw new RuntimeException(e);
		}
	}

	private static void throwDemo() throws IOException {
		throw new IOException("test IOException"); //checked exception
	}
	
	private static void sleepDemo() throws InterruptedException {
		Thread.sleep(1000);
	}

}
