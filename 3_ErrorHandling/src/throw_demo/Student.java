package throw_demo;

public class Student {
	private double mathMark;
	private double chemistryMark;

	public double getMathMark() {
		return mathMark;
	}

	public void setMathMark(double mathMark) throws InvalidMarkValueException{
		guardValidMark(mathMark);
		this.mathMark = mathMark;
	}

	public double getChemistryMark() {
		return chemistryMark;
	}

	public void setChemistryMark(double chemistryMark) throws InvalidMarkValueException{
		guardValidMark(chemistryMark);
		this.chemistryMark = chemistryMark;
	}

	public void guardValidMark(double mark) throws InvalidMarkValueException{
		if (mark < 0 || mark > 10) {
			throw new InvalidMarkValueException("mark must be between [0, 10]");
		}
	}
}
