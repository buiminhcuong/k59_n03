package exception_demo;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputDoubleDemo {

	public static void main(String[] args) {
		double d = readDouble("input a double number within range [0, 10000]: ", 0, 10000);
		System.out.println("You entered: " + d);
	}
	
	private static double readDouble(String message, double lowerBound, double upperBound) {
		Scanner sc = new Scanner(System.in);
		double result = 0;
		
		boolean isCorrect = true;
		do {
			System.out.print(message);
			try {
				result = sc.nextDouble();
			}
			catch(InputMismatchException ex) {
				System.out.println("Enter a valid number");
				sc.nextLine();
				isCorrect = false;
				continue;
			}
			
			isCorrect = isWithinBoundary(result, lowerBound, upperBound);
		} while (!isCorrect);

		return result;
	}

	private static boolean isWithinBoundary(double result, double lowerBound, double upperBound) {
		boolean isCorrect;
		if (result < lowerBound || result > upperBound) {
			System.out.println("Value out of range");
			isCorrect = false;
		}
		else {
			isCorrect = true;
		}
		return isCorrect;
	}

}
