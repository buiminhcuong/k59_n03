package exception_demo;

public class FinallyDemo {

	public static void main(String[] args) {
		testMethod();
	}

	private static int testMethod() {
		try {
			System.out.println("test method");
			return 10;
		} 
		finally {
			System.out.println("inside finally");
		}
	}

}
