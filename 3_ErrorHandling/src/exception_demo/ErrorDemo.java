package exception_demo;

public class ErrorDemo {

	public static void main(String[] args) {
		try {
			arrayDemo();
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("1");
		} 
		catch (ArithmeticException e) {
			System.err.println("2");
		}
		catch (Exception e) {
			System.err.println("3");
		}
		
		System.out.println("Finish");
	}

	private static void arrayDemo() {
		int[] arr = new int[3];
		arr[0] = 10;
		arr[1] = 11;
		arr[2] = 12;
		System.out.println(arr[4]);
	}
}
