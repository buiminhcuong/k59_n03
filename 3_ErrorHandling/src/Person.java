
public class Person {
	public String name;
	
	public void finalize() {
		System.out.println("finalized");
	}
	
	public static void main(String[] args) {
		Person p1 = new Person();
		Person p2 = new Person();
		p2 = p1;
		System.gc();
	}
}
