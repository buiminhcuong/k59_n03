package gui_demo;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class GridLayoutDemoFrame extends HelloWorldFrame {

	public GridLayoutDemoFrame() {
		super("GridLayout demo");
		
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(4, 3));
		add(centerPanel);
		
		for (int i = 0; i < 10; i++) {
			centerPanel.add(new JButton(String.valueOf(i)));
		}
	}

	public static void main(String[] args) {
		GridLayoutDemoFrame frame1 = new GridLayoutDemoFrame();
		frame1.setVisible(true);
	}
}
