package gui_demo;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GridBagLayoutDemo extends HelloWorldFrame{
	JPanel centerPanel = new JPanel();
	
	public GridBagLayoutDemo() {
		super("Grid bag layout demo");

		centerPanel.setLayout(new GridBagLayout());
		add(centerPanel);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.ipady = 5;
		c.insets = new Insets(5, 20, 5, 20);
		
		c.gridx = 0;
		c.gridy = 0;
		centerPanel.add(new JLabel("Username: "), c);
		
		c.gridx = 1;
		c.weightx = 3;
		centerPanel.add(new JTextField(20), c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		c.weightx = 1;
		centerPanel.add(new JButton("3"), c);
	}

	public static void main(String[] args) {
		GridBagLayoutDemo frame1 = new GridBagLayoutDemo();
		frame1.setVisible(true);
	}
}
