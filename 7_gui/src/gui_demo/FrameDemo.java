package gui_demo;

import java.awt.BorderLayout;

import javax.swing.JButton;

public class FrameDemo extends HelloWorldFrame {
	public FrameDemo() {
		super("Hello world");
		add(BorderLayout.CENTER,new JButton("CENTER"));
		add(BorderLayout.WEST, new JButton("WEST"));
		add(BorderLayout.EAST, new JButton("EAST"));
		add(BorderLayout.NORTH, new JButton("NORTH"));
		add(BorderLayout.SOUTH, new JButton("SOUTH"));	
	}

	public static void main(String[] args) {
		FrameDemo frame1 = new FrameDemo();
		frame1.setVisible(true);
	}
}
