package gui_demo;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PanelDemoFrame extends HelloWorldFrame{
	public PanelDemoFrame() {
		super("Panel demo");
		
		JPanel bottomPanel = new JPanel();
		add(BorderLayout.CENTER, bottomPanel);
		
		bottomPanel.add(new JLabel("test label"));
		bottomPanel.add(new JTextField(20));
		bottomPanel.add(new JButton("test button"));
	}

	public static void main(String[] args) {
		PanelDemoFrame frame1 =  new PanelDemoFrame();
		frame1.setVisible(true);
	}
}
