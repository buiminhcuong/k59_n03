package gui_demo;

import javax.swing.JFrame;

public class HelloWorldFrame extends JFrame{
	public HelloWorldFrame(String title) {
		setSize(600, 400);
		setTitle(title);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
