package event_demo;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import gui_demo.HelloWorldFrame;

public class ButtonClickDemoFrame extends HelloWorldFrame implements ActionListener{
	JPanel centerPanel = new JPanel();
	
	public ButtonClickDemoFrame() {
		super("Button click demo");
		add(centerPanel);
		JButton button1 = new JButton("Change bg color to red");
		button1.setActionCommand("red");
		button1.addActionListener(this);
		centerPanel.add(button1);
		
		JButton button2 = new JButton("Change bg color to blue");
		button2.setActionCommand("blue");
		button2.addActionListener(this);
		centerPanel.add(button2);
		
		JButton button3 = new JButton("Change bg color to green");
		button3.setActionCommand("green");
		button3.addActionListener(this);
		centerPanel.add(button3);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if("red".equals(cmd)) {
			centerPanel.setBackground(Color.RED);
		} else if("blue".equals(cmd)) {
			centerPanel.setBackground(Color.BLUE);
		} else if("green".equals(cmd)) {
			centerPanel.setBackground(Color.GREEN);
		}
	}
	
	public static void main(String[] args) {
		ButtonClickDemoFrame frame1 = new ButtonClickDemoFrame();
		frame1.setVisible(true);
	}
}
